package br.ucsal.bes20191.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Before;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;




public class AlunoBOUnitarioTest {
	private AlunoBO alunoBO;
	private AlunoDAO alunoDAO;
	private DateHelper dateUtil;
	private AlunoBuilder alunoBuilder;
	
	@Before
	public void setup(){
		alunoDAO = Mockito.mock(AlunoDAO.class);
		dateUtil = new DateHelper();
		alunoBO =  new AlunoBO(alunoDAO, dateUtil);
		alunoBuilder = new AlunoBuilder();
		alunoBO = new AlunoBO(alunoDAO, dateUtil);
		Mockito.when(alunoDAO.encontrarPorMatricula(1)).thenReturn(alunoBuilder.buildAluno1());
		 Mockito.when(alunoDAO.encontrarPorMatricula(2)).thenReturn(alunoBuilder.buildAluno2());
	}
	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	public void testarCalculoIdadeAluno1() {
		int idadeEsperada = 16;
		int id_matricula = 1;
		int idadeObtida = alunoBO.calcularIdade(id_matricula);
		Assert.assertEquals(idadeEsperada, idadeObtida);
	}
	
	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
		Aluno alunoNovo = alunoDAO.encontrarPorMatricula(2);
		alunoBO.atualizar(alunoNovo);
		Mockito.verify(alunoDAO).salvar(alunoNovo);

	}


}
