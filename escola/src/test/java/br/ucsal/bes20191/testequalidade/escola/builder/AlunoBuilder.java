package br.ucsal.bes20191.testequalidade.escola.builder;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
Aluno aluno;

public Aluno buildAluno1() {
    aluno = new Aluno();
    aluno.setMatricula(1);
    aluno.setAnoNascimento(2003);
    aluno.setNome("Cl�udio");
    aluno.setSituacao(SituacaoAluno.ATIVO);
    return aluno;
}

public Aluno buildAluno2() {
    aluno = new Aluno();
    aluno.setMatricula(2);
    aluno.setAnoNascimento(1996);
    aluno.setNome("Haroldo");
    aluno.setSituacao(SituacaoAluno.CANCELADO);
    return aluno;
}

}

